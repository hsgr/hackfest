short [en]:
Working on an open-source project? Give a lightning talk on the upcoming MicroHackFest and share your experience!
Δουλεύετε με κάποιο project ανοιχτού λογισμικού; Κάντε ένα lighthning talk στο επόμενο μhackfest και μοιραστείτε την εμπερία σας!


Για discuss
Subject:  μHackfest00 2020/01/28 : Call for Participation / κάλεσμα συμμετοχης:

[ENG]
μHackFest (aka microHackfest) is a monthly event at Hackerspace.gr showcasing open-source technologies, solutions and hacks.
Everybody is welcome to share their experiences, discuss ideas and interact with the community.
Want to showcase an open-source project you work with, develop, or hack on a 10-15' lightning talk at hackerspace.gr?
Reply to this message with a title and a link to a repo/project site/blog to a post/presentation.
μHackFest00 is scheduled for 28th January 2020 and has the trait of preparation before FOSDEM 2020.
The line-up will be finalised by the 20th of January!
PS: Beer Social Event upon the end of presentations.

[GR]
Το μHackFest (microhackfest) είναι ένα μηνιαίο event στο hackerspace.gr για να παρουσιάστε τεχνολογίες, λύσεις και hacks ανοιχτού κώδικα.
Όλοι είναι ευπρόσδεκτοι να μοιραστούν τις εμπειρίες τους, να συζητήσουν ιδέες και να αλληλεπιδράσουν με τη κοινότητα.
Αν επιθυμείτε να παρουσιάστε ένα project ανοιχτού κώδικα το οποίο, δουλεύετε, αναπτύσετε, ή hack-άρετε
απαντήστε σε αυτό το μήνυμα με το τίτλο και με link προς ένα repo/ project site/ άρθρο σε blog/ παρουσίαση
Tο μHackFest00 είναι προγραμματισμένο για την 28η Ιανουαρίου και έχει χαρακτήρα προετοιμασίας πριν τη FOSDEM 2020.
Oι συμμετοχές θα ανακοινωθούν την 20η Ιανουρίου.
ΥΣ: Βeer Social Event meτά το πέρας των παρουσιάσεων.